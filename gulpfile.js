const gulp = require('gulp');
const sourcemaps = require('gulp-sourcemaps');
const nodemon = require('gulp-nodemon');
const eslint = require('gulp-eslint');
const apidoc = require('gulp-apidoc');
const rollup = require('gulp-rollup');
const json = require('rollup-plugin-json');
const babel = require('rollup-plugin-babel');


gulp.task('lint', () => gulp.src(['./app/**/*.js', '!node_modules/**'])
  .pipe(eslint())
  .pipe(eslint.format()));

gulp.task('env', () => gulp.src('./app/.env')
  .pipe(gulp.dest('./dist/')));


gulp.task('assets', gulp.parallel('env'), () => gulp.src('*.*', { read: false })
  .pipe(gulp.dest('./dist/uploads/')));

gulp.task('documentation', done => apidoc({
  src: 'app/routes',
  dest: 'dist/doc/'
}, done));

gulp.task('bundle', (done) => {
  gulp.src('./app/**/*.js')
    .pipe(sourcemaps.init())
    // transform the files here.
    .pipe(rollup({
      // any option supported by Rollup can be set here.
      allowRealFiles: true,
      input: './app/server.js',
      plugins: [
        json({
          exclude: 'node_modules/**',
          preferConst: true
        }),
        babel({
          exclude: 'node_modules/**',
          babelrc: false
        })
      ],
      output: {
        format: 'cjs'
      }
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./dist'));
  done();
});

gulp.task('build', gulp.parallel('lint', 'assets', 'documentation', 'bundle'));
gulp.task('serve', gulp.series('build', () => nodemon({
  script: './dist/server.js',
  watch: 'app',
  tasks: ['build'],
  env: { NODE_ENV: 'development' }
})));
