import mongoose from 'mongoose';

let redisClient = null;

// Call init only once when the app opens.
// This makes sure only one connection is open to MongoDB
const init = (cb) => {
  const MONGO_URL = `mongodb://${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}`;
  mongoose.promise = Promise;
  mongoose.connect(MONGO_URL, {}, (err) => {
    process.nextTick(() => {
      cb(err);
    });
  });
};

// getInstance returns connected instance of mongoose
const getInstance = () => mongoose;

// getInstance returns connected instance of mongoose
const getRedis = () => redisClient;

// getObjectId
const getObjectId = id => mongoose.Types.ObjectId(id);

// getObjectId
const generateObjectId = () => mongoose.Types.ObjectId();

// Closes the connection to DB
const close = () => {
  mongoose.connection.close();
};


export default {
  init,
  getInstance,
  getRedis,
  getObjectId,
  generateObjectId,
  close
};
