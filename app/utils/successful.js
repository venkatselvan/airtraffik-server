/*
 * Check whether the result from mongoose is successful
 */
import { has } from 'lodash';

const isUpdated = result => result !== null && has(result, 'n') && result.n > 0;

const isRemoved = results => results !== null && has(results, 'result') && results.result.n > 0;

export {
  isUpdated,
  isRemoved
};
