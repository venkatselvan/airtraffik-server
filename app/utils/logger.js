import pino from 'pino';
import pkg from '../../package.json';

export default pino().child({ app: pkg.name });
