import bcrypt from 'bcryptjs';

const allowedRoutes = req => req.path.startsWith('/user/login')
  || req.path.startsWith('/user/signin')
  || ((req.path === '/user' || req.path === '/user/') && req.method === 'POST')
  || (req.path.startsWith('/user/forgotPass') && req.method === 'PUT')
  || (req.path.startsWith('/user/resetPass') && req.method === 'PUT')
  || ((req.path.startsWith('/product/visible') && req.method === 'GET'))
  || ((req.path.startsWith('/tag/shop') && req.method === 'GET'))
  || (req.path.startsWith('/analytics') && req.method === 'POST')
  || (req.path.startsWith('/lead') && req.method === 'POST')
  || ((req.path.startsWith('/docs') && req.method === 'GET'))
  || ((req.path.startsWith('/uploads') && req.method === 'GET'));

const canRouteBeAccessed = (user, path, method, callback) => {
  // TODO: Update the function to use User ACL
  if (path && method) {
    process.nextTick(() => {
      callback(null, true);
    });
  }
};

// eslint-disable-next-line arrow-body-style
const comparePassword = (plain, hashed) => {
  return new Promise((resolve, reject) => {
    // converts plain password text to hashed and compares it with existing password
    bcrypt.compare(plain, hashed, (err, isMatch) => {
      if (err) {
        reject(err);
      }
      resolve(isMatch);
    });
  });
};

export {
  allowedRoutes,
  canRouteBeAccessed,
  comparePassword
};
