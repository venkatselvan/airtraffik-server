import express from 'express';
import HttpStatus from 'http-status';
import * as Joi from 'joi';
import moment from 'moment';
import { has, isEmpty, each, find, orderBy, indexOf } from 'lodash';
import SlotAllocation from '../models/slotAllocation.js';
import logger from '../utils/logger';
import { isUpdated, isRemoved } from '../utils/successful';
import SlotRequest from '../models/slotRequest.js';

const AIRPORTS = {
  MAA: 5,
  BOM: 7,
  CCU: 5,
  IGI: 5
};

const AIRLINES = {
  'air-india': 56.6,
  'go-air': 75.9,
  vistara: 75.3,
  spicejet: 69.2,
  indigo: 64.0,
  'jet-airways': 63.1,
  'jet-lite': 63.1,
  'air-asia': 33.5
};

const SLOTBUFFER_TIME = {
  atr: 15,
  'b-737': 40,
  'a-320': 25
}; // In mins

function getDates(startDate, stopDate) {
  let dateArray = [];
  let currentDate = moment(startDate);
  let finalDate = moment(stopDate);
  while (currentDate <= finalDate) {
    dateArray.push(moment(currentDate).toDate());
    currentDate = moment(currentDate).add(1, 'days');
  }
  return dateArray;
}

const router = express.Router();


router.get('/', async (req, res) => {
  const q = req.query;
  const query = {};
  if (has(q, 'airport')) {
    query.$or = [{ departureFrom: q.airport }, { arrivalFrom: q.airport }];
  }
  try {
    const slotAllocations = await SlotAllocation.find(query).sort({ dateOfDept: 1, dateOfArrival: 1 }).lean().exec();
    return res.status(HttpStatus.OK).send({
      status: 'success', slotAllocations
    });
  } catch (err) {
    logger.error('[readAllSlotAllocation]', err);
    return res.status(HttpStatus.OK).send({ status: 'error', error: 'Error reading SlotAllocation.' });
  }
});


router.get('/:slotAllocationId', async (req, res) => {
  const _id = req.params.slotAllocationId;

  try {
    const slotAllocation = await SlotAllocation.find({ _id }).lean().exec();
    return res.status(HttpStatus.OK).send({ status: 'success', slotAllocation });
  } catch (err) {
    logger.error('[readAllSlotAllocations]', err);
    return res.status(HttpStatus.OK).send({ status: 'error', error: 'Error reading slotAllocations.' });
  }
});


router.post('/allocate', async (req, res) => {
  const b = req.body;

  const { airport } = req.query;
  const effectiveFrom = moment(req.query.effectiveFrom, 'DD-MM-YYYY').toDate();
  const effectiveTo = moment(req.query.effectiveTo, 'DD-MM-YYYY').toDate();

  const searchQuery = {
    effectiveFrom: { $gte: effectiveFrom },
    effectiveTo: { $lte: effectiveTo },
    $or: [{ departureFrom: airport }, { arrivalFrom: airport }]
  };
  const slotRequests = await SlotRequest.find(searchQuery).lean().exec();
  const otpBasedSR = orderBy(slotRequests, [function(o) { 
    const OTP = AIRLINES[o.airline] ? AIRLINES[o.airline] : 0;
    return OTP;
  }], ['desc']);
  const dates = getDates(effectiveFrom, effectiveTo);
  const slotAllocations = [];

  let typeOfAllocation = 'DAY_WISE';
  if (req.query.allocationType) {
    typeOfAllocation = req.query.allocationType;
  }

  const blockedIds = [];
  each(dates, (date) => {
    const dayOfWeek = date.getDay();
    each(otpBasedSR, (sr) => {
      const dateOfArrival = sr.timeOfArrival < sr.timeOfDept ? moment(date).add(1, 'days') : date;
      if (indexOf(sr.frequency, `${dayOfWeek}`) && date >= sr.effectiveFrom && date <= sr.effectiveTo) {
        let allocated = false;
        const bTime = SLOTBUFFER_TIME[sr.aircraftType];
        const dGates = AIRPORTS[sr.departureFrom];
        const aGates = AIRPORTS[sr.arrivalFrom];
        let departureGate = -1;
        let arrivalGate = -1;
        for (let i = 1; i <= dGates; i++) {
          const found = find(slotAllocations, function(sa) {
            return (sa.departureGate === i && sa.dateOfDept === date && sa.departureFrom === sr.departureFrom && (sa.timeOfDept >= (sr.timeOfDept - bTime) && sa.timeOfDept <= (sr.timeOfDept + bTime)));
          });

          if (!found) {
            if (typeOfAllocation === 'DAY_WISE') {
              departureGate = i;
              break;
            } else if (indexOf(blockedIds, sr._id) < 0) {
              departureGate = i;
              break;
            }
          } else if (typeOfAllocation !== 'DAY_WISE' && i === dGates.length) {
            blockedIds.push(sr._id);
          }
        }

        for (let i = 1; i <= aGates; i++) {
          const found = find(slotAllocations, function(sa) {
            return (sa.arrivalGate === i && sa.dateOfArrival === dateOfArrival && sa.arrivalFrom === sr.arrivalFrom && (sa.timeOfArrival >= (sr.timeOfArrival - bTime) && sa.timeOfArrival <= (sr.timeOfArrival + bTime)));
          });

          if (!found) {
            if (typeOfAllocation === 'DAY_WISE') {
              arrivalGate = i;
              break;
            } else if (indexOf(blockedIds, sr._id) < 0) {
              arrivalGate = i;
              break;
            }
          } else if (typeOfAllocation !== 'DAY_WISE' && i === aGates.length) {
            blockedIds.push(sr._id);
            break;
          }
        }

        if (departureGate > 0 && arrivalGate > 0) {
          allocated = true;
        }

        slotAllocations.push({
          flightNo: sr.flightNo,
          airline: sr.airline,
          operatorCode: sr.operatorCode,
          aircraftType: sr.aircraftType,
          dateOfDept: date,
          dateOfArrival: dateOfArrival,
          timeOfDept: sr.timeOfDept,
          timeOfArrival: sr.timeOfArrival,
          departureFrom: sr.departureFrom,
          arrivalFrom: sr.arrivalFrom,
          effectiveFrom: sr.effectiveFrom,
          effectiveTo: sr.effectiveTo,
          departureGate,
          arrivalGate,
          allocated
        });
      }
    });
  });

  try {
    const result = await SlotAllocation.create(slotAllocations);
    res.status(HttpStatus.OK).send({ status: 'success', result });
  } catch (err) {
    logger.error('[addSlotAllocation]', err);
    res.status(HttpStatus.OK).send({ status: 'error', error: 'Error adding slotAllocation details.' });
  }
});


router.put('/:slotAllocationId', async (req, res) => {
  const _id = req.params.slotAllocationId;
  const b = req.body;
  const q = req.query;

  // Validate Input
  const schema = Joi.object().keys({
    b: Joi.object().keys({
      flightNo: Joi.string().required(),
      airline: Joi.string().required(),
      operatorCode: Joi.string().required(),
      aircrafttype: Joi.string().required(),
      dateOfDept: Joi.string().required(),
      dateOfArrival: Joi.string().required(),
      effectiveFrom: Joi.string().required(),
      effectiveTo: Joi.string().required(),
      gate: Joi.string().required()
    })
  });
  const validation = Joi.validate({ b }, schema);
  if (validation.error) {
    logger.error('[updateProject]', validation.error);
    return res.status(HttpStatus.OK).send({ status: 'error', error: 'Incorrect input format' });
  }

  const toUpdate = {};
  if (has(b, 'flightNo')) {
    toUpdate.flightNo = b.flightNo;
  }
  if (has(b, 'airline')) {
    toUpdate.airline = b.airline;
  }
  if (has(b, 'operatorCode')) {
    toUpdate.operatorCode = b.operatorCode;
  }
  if (has(b, 'aircrafttype')) {
    toUpdate.aircrafttype = b.aircrafttype;
  }
  if (has(b, 'dateOfDept')) {
    toUpdate.dateOfDept = b.dateOfDept;
  }
  if (has(b, 'dateOfArrival')) {
    toUpdate.dateOfArrival = b.dateOfArrival;
  }
  if (has(b, 'gate')) {
    toUpdate.gate = b.gate;
  }
  if (has(b, 'effectiveFrom')) {
    toUpdate.effectiveFrom = b.effectiveFrom;
  }
  if (has(b, 'effectiveTo')) {
    toUpdate.effectiveTo = b.effectiveTo;
  }
  if (has(q, 'departureFrom')) {
    toUpdate.departureFrom = q.departureFrom;
  }
  if (has(q, 'arrivalFrom')) {
    toUpdate.arrivalFrom = q.arrivalFrom;
  }
  if (isEmpty(toUpdate)) {
    return res.status(HttpStatus.OK).send({ status: 'error', error: 'No changes to be saved' });
  }
  try {
    const result = await SlotAllocation.update({ _id }, { $set: toUpdate });
    if (isUpdated(result)) {
      return res.status(HttpStatus.OK).send({ status: 'success' });
    }
    return res.status(HttpStatus.OK).send({ status: 'error', error: 'No such project exists.' });
  } catch (err) {
    logger.error('[updateProject]', err);
    res.status(HttpStatus.OK).send({ status: 'error', error: 'Error updating project.' });
  }
});


router.delete('/', async (req, res) => {
  const { airport } = req.query;

  const effectiveFrom = moment(req.query.effectiveFrom, 'DD-MM-YYYY').toDate();
  const effectiveTo = moment(req.query.effectiveTo, 'DD-MM-YYYY').toDate();
  const searchQuery = {
    effectiveFrom: { $gte: effectiveFrom },
    effectiveTo: { $lte: effectiveTo },
    $or: [{ departureFrom: airport }, { arrivalFrom: airport }]
  };

  try {
    const result = await SlotAllocation.remove(searchQuery).lean().exec();
    if (isRemoved(result)) {
      return res.status(HttpStatus.OK).send({ status: 'success' });
    }
    return res.status(HttpStatus.OK).send({ status: 'error', error: 'No such slotAllocation exists.' });
  } catch (err) {
    logger.error('[deleteslotAllocations]', err);
    res.status(HttpStatus.OK).send({ status: 'error', error: 'Error deleting slotAllocation.' });
  }
});


export default router;
