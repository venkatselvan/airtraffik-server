import express from 'express';
import HttpStatus from 'http-status';
import * as Joi from 'joi';
import moment from 'moment';
import { has, isEmpty } from 'lodash';
import SlotRequest from '../models/slotRequest.js';
import logger from '../utils/logger';
import { isUpdated, isRemoved } from '../utils/successful';

const router = express.Router();


router.get('/', async (req, res) => {
  const q = req.query;
  const query = {};
  if (has(q, 'airport')) {
    query.$or = [{ departureFrom: q.airport }, { arrivalFrom: q.airport }];
  }
  try {
    const slotRequest = await SlotRequest.find(query).sort({ effectiveFrom: 1, effectiveTo: 1 }).lean().exec();
    return res.status(HttpStatus.OK).send({
      status: 'success', slotRequest
    });
  } catch (err) {
    logger.error('[readAllSlotRequest]', err);
    return res.status(HttpStatus.OK).send({ status: 'error', error: 'Error reading SlotRequest.' });
  }
});


router.get('/:slotRequestId', async (req, res) => {
  const _id = req.params.slotRequestId;
  const q = req.query.airport;
  const query = {};
  if (has(q, 'deptartureFrom')) {
    query.departureFrom = q.departureFrom;
  }
  if (has(q, 'arrivalFrom')) {
    query.arrivalFrom = q.arrivalFrom;
  }
  try {
    const slotRequest = await SlotRequest.find({ _id, query }).lean().exec();
    return res.status(HttpStatus.OK).send({ status: 'success', slotRequest });
  } catch (err) {
    logger.error('[readAllSlotRequests]', err);
    return res.status(HttpStatus.OK).send({ status: 'error', error: 'Error reading slotRequests.' });
  }
});


router.post('/', async (req, res) => {
  const b = req.body;

  /*
  // Validate Input
  const schema = Joi.object().keys({
    b: Joi.object().keys({
      flightNo: Joi.string().required(),
      airline: Joi.string().required(),
      operatorCode: Joi.string().required(),
      aircraftType: Joi.string().required(),
      dateOfDept: Joi.string().required(),
      dateOfArrival: Joi.string().required(),
      deptartureFrom: Joi.string().required(),
      arrivalFrom: Joi.string().required(),
      gate: Joi.string().required()
    })
  });
  const validation = Joi.validate({ b }, schema);
  if (validation.error) {
    logger.error('[addSlotRequest]', validation.error);
    return res.status(HttpStatus.OK).send({ status: 'error', error: validation.error });
  }
  */
  try {
    const result = await SlotRequest.create({
      flightNo: b.flightNo,
      airline: b.airline,
      frequency: b.frequency,
      operatorCode: b.operatorCode,
      aircraftType: b.aircraftType,
      effectiveFrom: b.effectiveFrom,
      effectiveTo: b.effectiveTo,
      departureFrom: b.departureFrom,
      timeOfDept: b.timeOfDept,
      timeOfArrival: b.timeOfArrival,
      arrivalFrom: b.arrivalFrom,
      gate: b.gate
    });
    res.status(HttpStatus.OK).send({ status: 'success', result });
  } catch (err) {
    logger.error('[addSlotRequest]', err);
    res.status(HttpStatus.OK).send({ status: 'error', error: 'Error adding slotRequest details.' });
  }
});


router.put('/:slotRequestId', async (req, res) => {
  const _id = req.params.slotRequestId;
  const b = req.body;

  // Validate Input
  const schema = Joi.object().keys({
    b: Joi.object().keys({
      flightNo: Joi.string().required(),
      airline: Joi.string().required(),
      operatorCode: Joi.string().required(),
      aircrafttype: Joi.string().required(),
      dateOfDept: Joi.string().required(),
      dateOfArrival: Joi.string().required(),
      gate: Joi.string().required()
    })
  });
  const validation = Joi.validate({ b }, schema);
  if (validation.error) {
    logger.error('[updateProject]', validation.error);
    return res.status(HttpStatus.OK).send({ status: 'error', error: 'Incorrect input format' });
  }

  const toUpdate = {};
  if (has(b, 'flightNo')) {
    toUpdate.flightNo = b.flightNo;
  }
  if (has(b, 'airline')) {
    toUpdate.airline = b.airline;
  }
  if (has(b, 'operatorCode')) {
    toUpdate.operatorCode = b.operatorCode;
  }
  if (has(b, 'aircrafttype')) {
    toUpdate.aircrafttype = b.aircrafttype;
  }
  if (has(b, 'dateOfDept')) {
    toUpdate.dateOfDept = b.dateOfDept;
  }
  if (has(b, 'dateOfArrival')) {
    toUpdate.dateOfArrival = b.dateOfArrival;
  }
  if (has(b, 'gate')) {
    toUpdate.gate = b.gate;
  }
  if (has(b, 'departureFrom')) {
    toUpdate.departureFrom = b.departureFrom;
  }
  if (has(b, 'arrivalFrom')) {
    toUpdate.arrivalFrom = b.arrivalFrom;
  }
  if (isEmpty(toUpdate)) {
    return res.status(HttpStatus.OK).send({ status: 'error', error: 'No changes to be saved' });
  }
  try {
    const result = await SlotRequest.update({ _id }, { $set: toUpdate });
    if (isUpdated(result)) {
      return res.status(HttpStatus.OK).send({ status: 'success' });
    }
    return res.status(HttpStatus.OK).send({ status: 'error', error: 'No such project exists.' });
  } catch (err) {
    logger.error('[updateProject]', err);
    res.status(HttpStatus.OK).send({ status: 'error', error: 'Error updating project.' });
  }
});

router.delete('/', async (req, res) => {
  const { airport } = req.query;

  const effectiveFrom = moment(req.query.effectiveFrom, 'DD-MM-YYYY').toDate();
  const effectiveTo = moment(req.query.effectiveTo, 'DD-MM-YYYY').toDate();
  const searchQuery = {
    effectiveFrom: { $gte: effectiveFrom },
    effectiveTo: { $lte: effectiveTo },
    $or: [{ departureFrom: airport }, { arrivalFrom: airport }]
  };

  try {
    const result = await SlotRequest.remove(searchQuery).lean().exec();
    if (isRemoved(result)) {
      return res.status(HttpStatus.OK).send({ status: 'success' });
    }
    return res.status(HttpStatus.OK).send({ status: 'error', error: 'No such slotRequest exists.' });
  } catch (err) {
    logger.error('[deleteAlllSlotRequest]', err);
    res.status(HttpStatus.OK).send({ status: 'error', error: 'Error deleting slotRequest.' });
  }
});


router.delete('/:slotRequestId', async (req, res) => {
  const _id = req.params.slotRequestId;

  // Validate Input
  const schema = Joi.object().keys({
    _id: Joi.string().required()
  });
  const validation = Joi.validate({ _id }, schema);
  if (validation.error) {
    logger.error('[deleteslotRequest]', validation.error);
    return res.status(HttpStatus.OK).send({ status: 'error', error: 'Incorrect input format' });
  }
  try {
    const result = await SlotRequest.remove({ _id });
    if (isRemoved(result)) {
      return res.status(HttpStatus.OK).send({ status: 'success' });
    }
    return res.status(HttpStatus.OK).send({ status: 'error', error: 'No such slotRequest exists.' });
  } catch (err) {
    logger.error('[deleteslotRequest]', err);
    res.status(HttpStatus.OK).send({ status: 'error', error: 'Error deleting slotRequest.' });
  }
});


export default router;
