import express from 'express';
import HttpStatus from 'http-status';
import * as Joi from 'joi';
import csv from 'fast-csv';
import path from 'path';
import fs from 'fs-extra';
import multer from 'multer';
import crypto from 'crypto';
import { each, has, isEmpty } from 'lodash';
import DB from '../utils/db';
import Tag from '../models/tag';
import logger from '../utils/logger';
import { isUpdated, isRemoved } from '../utils/successful';

const router = express.Router();

fs.mkdirs(path.join(__dirname, '/uploads/tags'));

const storage = multer.diskStorage({
  destination: (req, file, cb) => (
    cb(null, path.join(__dirname, '/uploads/tags'))
  ),
  filename: (req, file, cb) => {
    crypto.pseudoRandomBytes(16, (err, raw) => (
      cb(null, `${raw.toString('hex')}${Date.now()}${path.extname(file.originalname)}`)
    ));
  }
});

/**
 * @apiDefine ErrorBlock
 * @apiErrorExample {json} Error-Response:
 *     HTTP/1.1 200 Complete
 *     {
 *       "status": "error",
 *       "error": <i>Error message.</i>
 *     }
 * @apiErrorExample {json} Auth-Failed:
 *     HTTP/1.1 401 Unauthorized
 *     {
 *       "status": "error",
 *       "error": "UNAUTHORIZED"
 *     }
 */

/**
  * @api {get} /tag/shop/:shopId Fetch all tags by shopId
  * @apiName fetchAllTags
  * @apiGroup Tags
  *
  * @apiParam shopId Shop's Unique Id
  * @apiSuccessExample {json} Success-Response:
  *     HTTP/1.1 200 OK
  *     {
  *       "status": "success",
  *       "tag": [{
  *          "_id": "5a3deade22010",
  *          "name": "Tag Name",
  *          "parent": ["5a3ddadfade22015"]
  *       }]
  *     }
  * @apiError ErrorRetrieving
  * @apiUse ErrorBlock
  */

router.get('/shop/:shopId', async (req, res) => {
  const { shopId } = req.params;

  // Validate Input
  const schema = Joi.object().keys({
    shopId: Joi.string().required().error(() => res.status(HttpStatus.OK).send({ status: 'error', error: 'Shop ID Required/Invalid.' }))
  });
  const validation = Joi.validate({ shopId }, schema);
  if (validation.error) {
    logger.info('[fetchTagsByShopId]', validation.error);
    return res.status(HttpStatus.OK).send({ status: 'error', error: validation.error.message });
  }

  try {
    const tags = await Tag.find({ shopId }).lean().exec();
    res.status(HttpStatus.OK).send({ status: 'success', tags });
  } catch (err) {
    logger.info('[fetchTagsByShopId]', err);
    res.status(HttpStatus.OK).send({ status: 'error', error: 'Error reading tags.' });
  }
});

/**
 * @api {get} /tag/:tagId Fetch Tag with given Id
 * @apiName fetchTag
 * @apiGroup Tags
 *
 * @apiParam tagId Tag's unique ID
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "status": "success",
 *       "tag": {
 *         "_id": "5a3deade22010",
 *         "name": "Tag Name",
  *        "parent": ["5a3ddadfade22015"]
 *       }
 *     }
 * @apiError ErrorRetrieving
 * @apiUse ErrorBlock
 */

router.get('/:tagId', async (req, res) => {
  const _id = req.params.tagId;

  // Validate Input
  const schema = Joi.object().keys({
    _id: Joi.string().required().error(() => res.status(HttpStatus.OK).send({ status: 'error', error: 'Tag ID Required/Invalid.' }))
  });
  const validation = Joi.validate({ _id }, schema);
  if (validation.error) {
    logger.info('[fetchTag]', validation.error);
    return res.status(HttpStatus.OK).send({ status: 'error', error: validation.error.message });
  }

  try {
    const tag = await Tag.findOne({ _id }).lean().exec();
    if (!tag) {
      return res.status(HttpStatus.OK).send({ status: 'error', error: 'No such tag exists.' });
    }
    res.status(HttpStatus.OK).send({ status: 'success', tag });
  } catch (err) {
    logger.info('[fetchTag]', err);
    res.status(HttpStatus.OK).send({ status: 'error', error: 'Error reading tag.' });
  }
});

/**
 * @api {post} /tag/ Create Tag
 * @apiName createTag
 * @apiGroup Tags
 *
 * @apiHeaderExample {json} Input:
 *     {
 *       "name": "Tag Name",
 *       "parent": "5a3ddadfade22015"
 *     }
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "status": "success",
 *       "tag": {
 *          "_id": "5aed3dad0dka",
 *          "name": "Tag Name",
 *          "parent": ["5a3ddadfade22015"]
 *       }
 *     }
 *
 * @apiError ErrorRetrieving
 * @apiUse ErrorBlock
 */

router.post('/', async (req, res) => {
  const b = req.body;

  // Validate Input
  const schema = Joi.object().keys({
    b: Joi.object().keys({
      name: Joi.string().required().error(() => res.status(HttpStatus.OK).send({ status: 'error', error: 'Name Required/Invalid.' })),
      shopId: Joi.string().required().error(() => res.status(HttpStatus.OK).send({ status: 'error', error: 'Shop ID Required/Invalid.' })),
      parent: Joi.array().items(Joi.string()).error(() => res.status(HttpStatus.OK).send({ status: 'error', error: 'Parent Required/Invalid.' }))
    })
  });
  const validation = Joi.validate({ b }, schema);
  if (validation.error) {
    logger.info('[createTag]', validation.error);
    return res.status(HttpStatus.OK).send({ status: 'error', error: validation.error.message });
  }

  try {
    const toCreate = {
      name: b.name,
      shopId: b.shopId
    };
    if (has(b, 'parent')) {
      const parent = [];
      each(b.parent, (p) => {
        if (DB.getObjectId(p)) {
          parent.push(DB.getObjectId(p));
        }
      });
      toCreate.parent = parent;
    }
    const tag = await Tag.create(toCreate);

    res.status(HttpStatus.OK).send({
      status: 'success',
      tag
    });
  } catch (err) {
    logger.info('[createTag]', err);
    res.status(HttpStatus.OK).send({ status: 'error', error: 'Error creating tag.' });
  }
});

/**
 * @api {post} /tag/csv Create Tag From CSV
 * @apiName createTagFromCSV
 * @apiGroup Tags
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "status": "success"
 *     }
 *
 * @apiError ErrorRetrieving
 * @apiUse ErrorBlock
 */

router.post('/csv', multer({ storage }).any(), async (req, res) => {
  if (!req.files) {
    return res.status(HttpStatus.BAD_REQUEST).send({ status: 'error', error: 'No files were uploaded.' });
  }
  const file = req.files[0].path;
  const tags = [];

  csv
    .fromPath(file, {
      headers: true,
      ignoreEmpty: true
    })
    .on('data', (data) => {
      try {
        let name = data.name.trim();
        name = `${name[0].toUpperCase()}${name.substring(1)}`;
        const d = {
          name,
          shopId: DB.getObjectId(data.shopId)
        };

        d.parent = [];
        if (data.parent) {
          each(data.parent.split(','), (par) => {
            if (DB.getObjectId(par.trim())) {
              d.parent.push(DB.getObjectId(par.trim()));
            }
          });
        }
        tags.push(d);
      } catch (err) {
        logger.info('[createTagFromCSV]', err);
      }
    })
    .on('end', async () => {
      try {
        const docs = await Tag.create(tags);
        res.status(HttpStatus.OK).send({ status: 'success', added: docs.length, dropped: tags.length - docs.length });
      } catch (err) {
        logger.info('[createTagFromCSV]', err);
        res.status(HttpStatus.OK).send({ status: 'error', error: 'Error creating tag from csv.' });
      }
    });
});

/**
 * @api {put} /tag/:tagId Update tag
 * @apiName updateTag
 * @apiGroup Tags
 *
 * @apiHeaderExample {json} Input:
 *     {
 *       "name": "Tag Name" or
 *       "parent": ["5a3ddadfade22015"]
 *     }
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "status": "success"
 *     }
 * @apiError ErrorRetrieving
 * @apiUse ErrorBlock
 */

router.put('/:tagId', async (req, res) => {
  const p = req.params; // Get tag id from the params
  const b = req.body;

  // Validate Input
  const schema = Joi.object().keys({
    p: Joi.object().keys({
      tagId: Joi.string().required()
    }),
    b: Joi.object().keys({
      name: Joi.string().error(() => res.status(HttpStatus.OK).send({ status: 'error', error: 'Name Required/Invalid.' })),
      shopId: Joi.string().error(() => res.status(HttpStatus.OK).send({ status: 'error', error: 'Shop ID Required/Invalid.' })),
      parent: Joi.string().error(() => res.status(HttpStatus.OK).send({ status: 'error', error: 'Parent Required/Invalid.' }))
    })
  });
  const validation = Joi.validate({ p, b }, schema);
  if (validation.error) {
    logger.info('[updateTag]', validation.error);
    return res.status(HttpStatus.OK).send({ status: 'error', error: validation.error.message });
  }

  const toUpdate = {};
  if (has(b, 'name')) {
    toUpdate.name = b.name;
  }
  if (has(b, 'shopId')) {
    toUpdate.shopId = b.shopId;
  }
  if (has(b, 'parent')) {
    const parent = [];
    each(b.parent, (par) => {
      if (DB.getObjectId(par)) {
        parent.push(DB.getObjectId(par));
      }
    });
    toUpdate.parent = parent;
  }

  if (isEmpty(toUpdate)) {
    return res.status(HttpStatus.OK).send({ status: 'error', error: 'No changes to be saved' });
  }
  try {
    const result = await Tag.update({ _id: p.tagId }, { $set: toUpdate });
    if (isUpdated(result)) {
      return res.status(HttpStatus.OK).send({ status: 'success' });
    }
    return res.status(HttpStatus.OK).send({ status: 'error', error: 'No such tag exists.' });
  } catch (err) {
    logger.info('[updateTag]', err);
    res.status(HttpStatus.OK).send({ status: 'error', error: 'Error updating tag.' });
  }
});

/**
 * @api {delete} /tag/:tagId Delete tag
 * @apiName deleteTag
 * @apiGroup Tags
 *
 * @apiSuccessExample {json} Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "status": "success"
 *     }
 * @apiError ErrorRetrieving
 * @apiUse ErrorBlock
 */

router.delete('/:tagId', async (req, res) => {
  const p = req.params; // Get tag id from the params

  // Validate Input
  const schema = Joi.object().keys({
    p: Joi.object().keys({
      tagId: Joi.string().required().error(() => res.status(HttpStatus.OK).send({ status: 'error', error: 'Tag ID Required/Invalid.' }))
    })
  });
  const validation = Joi.validate({ p }, schema);
  if (validation.error) {
    logger.info('[deleteTag]', validation.error);
    return res.status(HttpStatus.OK).send({ status: 'error', error: validation.error.message });
  }

  const tagId = DB.getObjectId(p.tagId);
  if (!tagId) {
    return res.status(HttpStatus.OK).send({ status: 'error', error: 'Invalid tagId.' });
  }

  try {
    // Reset other Tag's parent id before removing.
    await Tag.update({ parent: tagId }, { $set: { parent: null } }, { multi: true });
    const result = await Tag.remove({ _id: tagId });
    if (isRemoved(result)) {
      return res.status(HttpStatus.OK).send({ status: 'success' });
    }
    return res.status(HttpStatus.OK).send({ status: 'error', error: 'No such tag exists.' });
  } catch (err) {
    logger.info('[deleteTag]', err);
    res.status(HttpStatus.OK).send({ status: 'error', error: 'Error deleting tag.' });
  }
});

export default router;
