import http from 'http';
import express from 'express';
import path from 'path';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import cors from 'cors';
import dotenv from 'dotenv';
import logger from './utils/logger.js';
import DB from './utils/db.js';

import slotAllocation from './routes/slotAllocation.js';
import slotRequest from './routes/slotRequest.js';

/*
 * Loads the environment variables from .env to process.env
 */
const envLoaded = dotenv.config({ path: `${__dirname}/.env` });
if (envLoaded.error) {
  logger.error('Unable to load .env, please check if it exists');
  process.exit();
}

/*
 * Initiates MongoDB connection, which
 * can be accessed in other files by DB.getInstance()
 */
DB.init((err) => {
  if (err) {
    throw err;
  }
});


/*
 * Initiates Express and loads following middlewares
 *  cors: Handles Cross Origin Request
 *  bodyParser: Converts chunks of inputs to req.body
 *  morgan: Logs each incoming http request
 */
const app = express();

// TODO: Set the origin to domain name of the servers calling API. Eg: origin: ['http://example.com', 'http://example2.com']
app.use(cors({ credentials: true, origin: '*' }));

app.use(morgan('short'));
// basically tells the system that you want json to be used.
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/docs', express.static(path.join(__dirname, process.env.SERVER_STATIC)));
app.use('/uploads', express.static(path.join(__dirname, 'uploads')));


/*
 * Define custom routes below
 */
app.use('/slotallocation', slotAllocation);
app.use('/slotrequest', slotRequest);

/*
 * Server is created with the provided port
 */
http.createServer(app).listen(process.env.SERVER_PORT, (err) => {
  if (err) {
    logger.error(err);
    process.exit();
  } else {
    logger.info('SERVER STARTED');
    logger.info(`Server listening on port ${process.env.SERVER_PORT}`);
  }
});
