import DB from '../utils/db.js';

const mongoose = DB.getInstance();
const { Schema } = mongoose;

const SlotRequestSchema = new Schema({
  flightNo: String,
  airline: String,
  operationCode: String,
  aircraftType: String,
  frequency: [String],
  effectiveFrom: Date,
  effectiveTo: Date,
  timeOfDept: Number,
  timeOfArrival: Number,
  departureFrom: String,
  arrivalFrom: String,
  gate: String
}, { timestamps: true });

export default mongoose.model('SlotRequest', SlotRequestSchema);
