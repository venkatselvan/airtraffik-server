import DB from '../utils/db.js';

const mongoose = DB.getInstance();
const { Schema } = mongoose;

const SlotSchema = new Schema({
  flightNo: String,
  airline: String,
  operationCode: String,
  aircraftType: String,
  frequency: String,
  dateOfDept: Date,
  dateOfArrival: Date,
  departureFrom: String,
  timeOfDept: Number,
  timeOfArrival: Number,
  arrivalFrom: String,
  departureGate: Number,
  arrivalGate: Number,
  effectiveFrom: Date,
  effectiveTo: Date,
  allocated: Boolean
}, { timestamps: true });

export default mongoose.model('Airport', SlotSchema);
