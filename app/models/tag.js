import DB from '../utils/db.js';

const mongoose = DB.getInstance();
const { Schema } = mongoose;

const TagSchema = new Schema({
  name: String,
  shopId: Schema.Types.ObjectId,
  parent: [Schema.Types.ObjectId]
}, { timestamps: true });

export default mongoose.model('Tag', TagSchema);
